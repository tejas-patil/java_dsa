	

	interface Demo1{
		
		static void fun(){
			
			System.out.println("In Fun Demo1");
		}
	}
	
	
	interface Demo2{
		
		static void fun(){
			
			System.out.println("In Fun Demo2");
		}
	}
	
	class Child implements Demo1,Demo2{


	// Error		Demo1.fun();
	// Error		Demo2.fun();
		
		  public void fun(){

			System.out.println("In Fun");
			Demo1.fun();
			Demo2.fun();
		}
	}

	class Client{
		
		public static void main (String [] tejas){
		
			Child obj = new Child();
			obj.fun();

	/*		Demo1 obj1 = new Child();
			obj1.fun();

			Demo2 obj2 = new Child();
			obj2.fun();*/
		
		}
	
	
	}
