


	
	interface Demo1{
		
			default void gun(){
			
			System.out.println("In Fun Demo");
		}
	}

	
	interface Demo2{
		
			default void gun(){
			
			System.out.println("In Fun Demo");
		}
	}
	
	class Child implements Demo1,Demo2{
		
		public void gun(){
			
			System.out.println("In Fun ChildDemo");
		}
	}

	class Client{

		public static void main (String [] tejas){

		Demo1 obj = new Child();
		obj.gun();


		Demo2 obj1 = new Child();
		obj1.gun();
		
		
		} 
	}
