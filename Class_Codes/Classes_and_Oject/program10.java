
class Demo{
	
	int x=10;
	static int y=20;

	static{
			
		System.out.println("Static Block 1");
		System.exit();
	}			
	public static void main(String [] tejas){
				
		System.out.println("In Main");
		Demo obj = new Demo();
    	System.out.println(obj.x);
	}
	static{
		System.out.println("Static Block 2");		
	}

}
