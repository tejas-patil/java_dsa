
import java.util.*;

class CricPlayer{
	
	
	int jerNo =0;
	String name = null;

	CricPlayer( int jerNo , String name){
		
		this.jerNo = jerNo;
		this.name = name;
	}

	public String toString (){
		
		return (jerNo+" "+name);
	}
}

class ArrayListDemo extends ArrayList{

	@SuppressWarnings("unchecked")
	
	public static void main (String [] tejas){
		
		ArrayList AL = new ArrayList();

		AL.add ( new CricPlayer( 18,"virat"));
		AL.add ( new CricPlayer( 7,"Dhoni"));

		System.out.println(AL);
	}
}
