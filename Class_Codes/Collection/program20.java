
import java.util.*;

class Movies {
	
	String movieName = null;
	double totColl = 0.0;
	float IMDB = 0.0f;

	Movies (String movieName, double totColl, float IMDB){
		
		this.movieName = movieName;
		this.totColl = totColl;
		this.IMDB = IMDB;
	}

	public String toString(){
		
		return (movieName +":"+  totColl+ ":" +  IMDB );
	}
}

class SortByName implements Comparator{
	
	public int compare(Object obj1 , Object obj2){
		
		return (((Movies)obj1).movieName.compareTo(((Movies)obj2).movieName));
	}
}

class SortBytotColl implements Comparator{

	
	public int compare(Object obj1, Object obj2){
		
		return (int)(((Movies)obj1).totColl -(((Movies)obj2).totColl));
	}
}

class SortByIMDB implements Comparator{
	
	public int compare(Object obj1, Object obj2){
		
		return(int)(((Movies)obj1).IMDB -(((Movies)obj2).IMDB));
	}
}

class UserList{
	
	public static void main(String [] tejas){
		
		ArrayList al = new ArrayList();

		al.add(new Movies("Gadar", 200.00, 8.8f));
		al.add(new Movies("OMG" , 150.00, 7.7f));
		al.add(new Movies("Ved",75.00,9.4f));

		Collections.sort(al,new SortByName());
		System.out.println(al);

		Collections.sort(al,new SortBytotColl());

		System.out.println(al);

		Collections.sort(al, new SortByIMDB());
		System.out.println(al);
	}




















}
