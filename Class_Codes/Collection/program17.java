
import java.util.*;

class Movies implements Comparable{
	
	String movieName = null;
	float totColl = 0.0f;

	Movies ( String movieName , float totColl){
		
		this.movieName= movieName;
		this.totColl= totColl;

	}

	public int compareTo(Object obj){
		
		return (movieName.compareTo(((Movies)obj).movieName));

	}
	public String toString (){
		 
		return movieName + totColl;
	}
}

class TreeSetDemo {
	
	public static void main  (String [] tejas){
		
		TreeSet ts = new TreeSet();
		
		ts.add(new Movies("Gadar" ,150.0f ));
		ts.add(new Movies("OMG 2" ,120.0f ));
		ts.add(new Movies("Jailer" ,250.0f ));

		System.out.println(ts);
		
	}
}
