

import java.util.*;

class CursorDemo{
	public static void main (String [] tejas){
		
		Vector v = new Vector();
		v.add("Ashish");
		v.add("Rahul");
		v.add("Kanha");
		v.add("Badhe");

		Enumeration Cursor = v.elements();

		System.out.println(Cursor.getClass().getName());

		while (Cursor.hasMoreElements()){
			
			System.out.println(Cursor.nextElement());
		}
	}
}
