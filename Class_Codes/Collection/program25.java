
import java.util.*;

class IdentityHashDemo{

	public static void main (String [] tejas){
		
		IdentityHashMap hm = new IdentityHashMap();

		hm.put(10,"Kanha");
		hm.put(10,"Rahul");
		hm.put(10,"Badhe");

		System.out.println(hm);

		IdentityHashMap hm1 = new IdentityHashMap();

		hm1.put(new Integer(10),"Kanha");
		hm1.put(new Integer(10),"Rahul");
		hm1.put(new Integer(10),"Badhe");

		System.out.println(hm1);
	}
}
