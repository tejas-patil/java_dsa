
import java.util.*;

class HashMapDemo{
	
	public static void main (String [] tejas){
		
		HashSet hs = new HashSet();

		hs.add("Kanha");
		hs.add("Ashish");
		hs.add("Badhe");
		hs.add("Rahul");

		System.out.println(hs);

		HashMap hm = new HashMap();

		hm.put("Kanha","Infosys");
		hm.put("Ashish", "Barclays");
		hm.put("Badhe","CarPro");
		hm.put("Rahul","BMC");

		System.out.println("Kanha".hashCode());
		System.out.println("Ashish".hashCode());
		System.out.println("Badhe".hashCode());
		System.out.println("Rahul".hashCode());
	

		System.out.println(hm);
	}
}
