

import java.util.*;

class HashSetDemo {
	
	public static void main (String [] tejas){
		
		HashSet hs = new HashSet();

		hs.add("Ram");
		hs.add("Sam");
		hs.add("Kam");

		System.out.println("Ram".hashCode());
		System.out.println("Sam".hashCode());
		System.out.println("Kam".hashCode());

		System.out.println(hs);
	}
}
