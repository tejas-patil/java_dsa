
	// Default Exception Handler 
	
class Demo{
	
	void m1(){
		
		System.out.println("In M1");
	}

	void m2(){
		
		System.out.println("In M2");

	}
}

class Client {
	
	public static void main (String [] tejas){
		
		Demo obj = new Demo();
		System.out.println("Start Main");
		
		obj.m1();
		obj=null;
		obj.m2();

		System.out.println("End Main");
		
	}
}

	// EGxception in thread "Main ": java.lang.NullPointerException : / by zero
//		at Demo.m1 (Prog3.java 12) 
//		at client.main(prog3.java 20) 	
