
	// Default Exception Handler 
	
class Demo{
	
	void m1(int x){
		
		System.out.println(x++);
		m1(x);
	}

}

class Client {
	
	public static void main (String [] tejas){
		
		Demo obj = new Demo();
		System.out.println("Start Main");
		
		obj.m1(10);

		System.out.println("End Main");
		
	}
}

	// EGxception in thread "Main ": java.lang.StackOverflowError
