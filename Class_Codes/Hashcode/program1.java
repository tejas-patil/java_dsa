class Demo{

	public static void main (String [] Args){
		String str1= "Shashi";
		String str2 =new String("Shashi");
		String str3= "Shashi";
		String str4= new String ("Shashi");

		System.out.println( str1.hashCode() );
		System.out.println( str2.hashCode() );
		System.out.println( str3.hashCode() );
		System.out.println( str4.hashCode() );
	}
}

// Here in Hash code obj created on heap section. In these code 2 Strings ( Shashi) is in scp and two created new obj.
//  on heap section. But they had same hashCode. 
