
import java.util.concurrent.*;

class MyThread implements Runnable{
	
	int num =0;

	MyThread( int num){
		
		this.num = num;
	}

	public void run (){
		
		System.out.println(Thread.currentThread() + " StartThread "+ num );

			Thread.currentThread().setPriority(8);

		System.out.println(Thread.currentThread().getPriority());
			
	
		dailyTask();

		System.out.println(Thread.currentThread() + " EndThread "+ num );
	}

	void dailyTask(){
		
		try {
			Thread.sleep(2000);
		} catch ( InterruptedException ie){}
	}
}

class ThreadDemoPool {
	
	public static void main (String [] tejas){
	
		ExecutorService	exs = Executors.newFixedThreadPool(5);

			for (int i=1; i<10 ; i++){
			
			MyThread t1 = new MyThread(i);

 				exs.execute(t1);
		}
			exs.shutdown();

	}
}
