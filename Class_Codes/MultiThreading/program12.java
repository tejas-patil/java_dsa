
class MyThread extends Thread{
	
	MyThread(String str){
		
		super(str);
	}
	
	MyThread(){
	
	}
	
	public void run(){
		
		//System.out.println(getName());
		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadGroupDemo{
	
	public static void main (String [] tejas){
		
		MyThread obj = new MyThread("ABC");
		obj.start();

		MyThread obj1 = new MyThread("XYZ");
		obj1.start();

		MyThread obj2 = new MyThread();
		obj2.start();
	}

}
