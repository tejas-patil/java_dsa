
class MyThread implements Runnable{
	
	public void run(){
		
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(5000);
		} catch (InterruptedException ie){}
	}
}

class Client{
	
	public static void main (String [] tejas){
		
		ThreadGroup pThreadGp = new ThreadGroup ("India");

		ThreadGroup cThreadGp = new ThreadGroup (pThreadGp , "Mumbai");

		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		System.out.println(pThreadGp.activeGroupCount());

		Thread t1= new Thread(pThreadGp, obj1 , "Maha");
		Thread t2 = new Thread(pThreadGp, obj2, "Goa");

		t1.start();
		t2.start();


		System.out.println(Thread.activeCount());
	
		}
}
