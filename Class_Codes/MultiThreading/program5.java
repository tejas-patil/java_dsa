	

class MyThread implements Runnable{
	
	public void run (){
		
		System.out.println("In MyThread");
		
		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadDemo{
	
	public static void main ( String [] tejas){
		
		MyThread obj = new MyThread();
		Thread obj1 = new Thread(obj);
		obj1.start();
		try{
		obj1.start();// IllegalThreadState Exception
		} catch ( IllegalThreadStateException obj2){
		
		System.out.println("Illegal Thread State Exception");
		}

		System.out.println(Thread.currentThread().getName());
	}
}
