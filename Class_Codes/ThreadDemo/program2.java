
class MyThread extends Thread {
	
	MyThread( ThreadGroup tg, String str){
		
		super (tg,str);
	}

	public void run(){
		
		System.out.println(getName());
		System.out.println(getThreadGroup());


		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{
	
	public static void main (String [] tejas){

		ThreadGroup pTG = new ThreadGroup ("Java");
		
		MyThread obj = new MyThread(pTG, "C2W");
		obj.start();
	}
}
