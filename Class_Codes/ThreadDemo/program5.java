
import java.util.concurrent.*;

class MyThread implements Runnable{
		
	int num =0;

	MyThread(int num){
		
		this.num = num;
	}
	public void run(){
		
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{
	
	public static void main(String [] tejas){
		
		ExecutorService exe = Executors.newCachedThreadPool();
		
		for (int i = 0; i<5 ; i++){
			
			MyThread obj = new MyThread(i);
			exe.execute(obj);
		} 
	}
}
