

class MyThread extends Thread{
	
	public void run(){
		
		Thread t = Thread.currentThread();
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{
	
	public static void main (String [] tejas){
		
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());

		MyThread obj = new MyThread();
		obj.start();
	
		t.setPriority(8);

	
		MyThread obj1 = new MyThread();
		obj1.start();
	}
}
