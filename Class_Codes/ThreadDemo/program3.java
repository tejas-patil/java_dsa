
class  MyThread extends Thread {
	
	MyThread (ThreadGroup tg , String str ){
		
		super (tg,str);
	}

	public void run (){
		
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(5000);
		} catch (InterruptedException ie  ){
			
			System.out.println (ie.toString());
		}
	}
}

class ThreadGroupDemo {
	
	public static void main (String [] tejas ) throws InterruptedException {

		ThreadGroup pTG = new ThreadGroup ("India");

		MyThread t1 = new MyThread (pTG , "Maha");
		MyThread t2 = new MyThread (pTG , "Goa");

		t1.start();
		t2.start();
		

	}
}
