

	class Parent{
	
		static void fun (){
			
			System.out.println("In Parent Fun");
		}
	}

	class Child extends Parent{
		
		void fun(){
			
			System.out.println("In Child Fun");
		}
	}
