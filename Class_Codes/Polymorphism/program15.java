

	class Parent{

		Object fun(){
			
			System.out.println("In Object Fun");
			return new Object ();
		
		}
	}

	class Child extends Parent{
	
		String fun(){

			super.fun();
		
			System.out.println("In Object Fun");
			return "C2W";
		}
	}

	class Client{
	
	public static void main (String [] tejas){
	
	Parent obj = new Child();
	obj.fun();

	}
	
	}
