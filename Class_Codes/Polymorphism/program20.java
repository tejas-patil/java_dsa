
	// Method Hiding 
    
	class Parent{
	
		static void fun(){
		
			System.out.println("In Parent Fun");
		}
	}

	class Child extends Parent{
	
		static void fun(){
			
			System.out.println("In Child Fun");
		}

		public static void main (String [] tejas){
			Parent obj= new Child();
			obj.fun();
		} 
	}
