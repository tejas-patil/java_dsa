/* Write a program ,take 10 input from the user and print only elements that are divisible by 5. */

import java.io.*;

class ArrayDemo{
	public static void main(String[]tejas)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Length of Array:");
		int size =Integer.parseInt(br.readLine());
		int arr []= new int[size];
		
		System.out.println("Enter Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i] =Integer.parseInt(br.readLine());	

		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]%5==0)				
				System.out.println("Number in array divisible by 5 is "+arr[i]);
		}
	}
}
