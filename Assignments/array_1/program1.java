/*WAP to take size of array from user and also take integer elements from user 
Print sum of odd elements only.  */

import java.io.*;

class ArrayDemo{
	public static void main(String[]tejas)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Length of Array:");
		int size =Integer.parseInt(br.readLine());
		int arr []= new int[size];
		
		System.out.println("Enter Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i] =Integer.parseInt(br.readLine());	

		}
		int sum=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2!=0)
				sum=sum+arr[i];
		}
		System.out.println("Sum of Odd element:"+sum);
	}
}
