/* Write a program, take 7 characters as an input , Print only vowels from the array */

import java.io.*;

class ArrayDemo{
	public static void main(String[]tejas)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Length of Array:");
		int size =Integer.parseInt(br.readLine());
		char arr []= new char[size];
		
		System.out.println("Enter Character Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i] =(char)br.read();
			br.skip(1);
		}

		System.out.println("Vowels in Array:");
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'||arr[i]=='A'|| arr[i]=='e' || arr[i]=='E' || arr[i]=='i' || arr[i]=='I' || arr[i]=='o' || arr[i]=='O' || arr[i]=='u' || arr[i]=='U')
				System.out.println(arr[i]);
		}
	}
}
