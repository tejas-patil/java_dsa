/* Write a program, take 7 characters as an input , Print only vowels from the array */

import java.util.*;

class ArrayDemo{
	public static void main(String[]tejas){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter length of array:");
		int size =sc.nextInt();
		char arr []= new char[size];

		System.out.println("Enter Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i] =sc.next().charAt(0);
		}

		System.out.println("Vowels in array:");
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'||arr[i]=='A'|| arr[i]=='e' || arr[i]=='E' || arr[i]=='i' || arr[i]=='I' || arr[i]=='o' || arr[i]=='O' || arr[i]=='u' || arr[i]=='U')
				System.out.println(arr[i]);
		}
	}
}
