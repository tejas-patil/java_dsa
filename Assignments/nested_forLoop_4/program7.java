/*	F
 *	E 1
 *	D 2 E
 *	C 3 D 4
 *	B 5 C 6 D
 *	A 7 B 8 C 9
 */

class Nested{
	public static void main(String[]tejas){
		int rows=6;
		char ch1='F';
		char ch2;
		int num=1;
		for(int i=1;i<=rows;i++){
			ch2=ch1;
			for(int j=1;j<=i;j++){
				if(j%2!=0){
					System.out.print(ch2+" ");
					ch2++;
				}
				else{
					System.out.print(num+" ");
					num++;
				}
			}
			ch1--;
			System.out.println();
		}
	}
}
