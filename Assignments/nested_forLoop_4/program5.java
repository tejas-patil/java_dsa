/*	A B C D
 *	B C D
 *	C D
 *	D
 */

class Nested{
	public static void main(String[]tejas){
		int rows=4;
		char ch1='A';
		char ch2;
		for(int i=1;i<=rows;i++){
			ch2=ch1;
			for(int j=i;j<=rows;j++){
				System.out.print(ch2+" ");
				ch2++;
			}
			ch1++;
			System.out.println();
		}
	}
}
