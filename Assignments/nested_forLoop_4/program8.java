/*	10
 *	I  H
 *	7  6 5
 *	D  C B A
 */

class Nested{
	public static void main(String[]tejas){
		int rows=4;
		int num=10;
		char ch='J';
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				if(i%2!=0){
					System.out.print(num+" ");
				}
				else{
					System.out.print(ch+" ");
				}
				ch--;
				num--;
			}
			System.out.println();
		}
	}
}
