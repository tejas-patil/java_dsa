/*	1
 *	8  9
 *	27 16 125
 *	64 25 216 49
 */

class Nested{
	public static void main(String[]tejas){
		int rows=4;
		int num1=1;
		int num2;
		for(int i=1;i<=rows;i++){
			num2=num1;
			for(int j=1;j<=i;j++){
				if(j%2 != 0){
					System.out.print(num2*num2*num2 +" ");
				}else{
					System.out.print(num2*num2 +" ");
				}
				num2++;
			}
			num1++;
			System.out.println();
		}
	}
}
