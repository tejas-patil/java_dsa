/*	1 2 3 4
 *	2 3 4
 *	3 4
 *	4
 */

class Nested{
	public static void main(String[]tejas){
		int rows=4;
		int num;
		for(int i=1;i<=rows;i++){
			num=i;
			for(int j=i;j<=rows;j++){
				System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}
}
