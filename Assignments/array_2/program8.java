/* WAP to find the uncommon elements between two arrays. */

import java.io.*;

class ArrayDemo{
	public static void main(String[]tejas)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Eneter Size of Array:");
		int size = Integer.parseInt(br.readLine());
		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("Enter Elements for Array-1:");
		for(int i=0;i<size;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter Elements for Array-2:");
		for(int i=0;i<size;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		int count1=0;
		int count2=0;
		System.out.println("Uncommon Elements:");
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					count1=1;
					break;
				}
			}
			if(count1==0)
				System.out.println(arr1[i]);
		}
		for(int i=0;i<arr2.length;i++){
			for(int j=0;j<arr1.length;j++){
				if(arr2[i]==arr1[j]){
					count2=1;
					break;
				}
			}
			if(count2==0)
				System.out.println(arr2[i]);
		}
	}
}
