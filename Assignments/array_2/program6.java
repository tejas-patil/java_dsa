/* WAP to take size of array from user and also take integer elements from user
find the maximum element from the array */

import java.io.*;

class ArrayDemo{
	public static void main(String[]tejas)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Eneter Size of Array:");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int max=arr[0];
		for(int i=0;i<arr.length;i++){
			if(max<arr[i]){
				max=arr[i];
			}
				
		}
				System.out.println("Max Element:"+max);
	}
}
