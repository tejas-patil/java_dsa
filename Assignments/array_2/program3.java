/* Write a Java program to find the sum of even and odd numbers in an array. */


import java.io.*;

class ArrayDemo{
	public static void main(String[]tejas)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Eneter Size of Array:");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int evensum=0;
		int oddsum=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0)
				evensum=evensum+arr[i];
			else
				oddsum=oddsum+arr[i];
		}
		System.out.println("Sum of Even Elements:"+evensum);
		System.out.println("Sum of Odd Elements:"+oddsum);
	}
}
