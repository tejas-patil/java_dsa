/* WAP to print the elements whose addition of digits is even. */

import java.io.*;

class ArrayDemo{
	public static void main(String[]tejas)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Eneter Size of Array:");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int x;
		System.out.println("Sum of these number is even:");
		for(int i=0;i<size;i++){
			x=arr[i];
			int sum=0;
			while(x!=0){
				int rem=x%10;
				sum=sum+rem;
				x=x/10;
			}
			if(sum%2==0)
				System.out.println(arr[i]);
		}
	}
}
