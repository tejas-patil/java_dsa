/* WAP to find the common elements between two arrays. */


import java.io.*;

class ArrayDemo{
	public static void main(String[]tejas)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Eneter Size of Array:");
		int size = Integer.parseInt(br.readLine());
		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("Enter Elements for Array-1:");
		for(int i=0;i<size;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter Elements for Array-2:");
		for(int i=0;i<size;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Common Elements:");
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				if(arr1[i]==arr2[j]){
					System.out.println(arr1[i]);
				}				
			}
		}
	}
}
