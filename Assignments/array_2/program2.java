/* WAP to find the number of even and odd integers in a given array of integers */

import java.io.*;

class ArrayDemo{
	public static void main(String[]tejas)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Eneter Size of Array:");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int evencount=0;
		int oddcount=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0)
				evencount++;
			else
				oddcount++;
		}
		System.out.println("Number of Even Elements:"+evencount);
		System.out.println("Number of Odd Elements:"+oddcount);
	}
}
