import java.io.*;

class MagicSession{
	public static void main(String []tejas)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Rows:");
		int rows = Integer.parseInt(br.readLine());

		System.out.println("Enter cols:");
		int cols = Integer.parseInt(br.readLine());

		System.out.println("Enter start Character");
		String str1 =br.readLine();
		char start= str1.charAt(0);

		System.out.println("Enter End Character");
		String str2 =br.readLine();
		char end= str2.charAt(0);
		
		for(int i=1;i<=rows;i++){
			int N=rows;
			for(int j=1;j<=cols;j++){
				if(i%2!=0){
					System.out.print(end+""+N+" ");
					N--;
				}
				else{
					System.out.print(start+""+j+" ");
				}
			}
			System.out.println();
		}
	}
}
