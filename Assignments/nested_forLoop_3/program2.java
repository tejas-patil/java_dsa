/*	1
 *	2 3
 *	4 5 6
 *	7 8 9 10
 */

class Nested{
	public static void main(String[]tejas){
		int rows=4;
		int num=1;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}
}
