/*	1
 *	8   27
 *	64  125 216
 *	343 512 749 1000
 */

class Nested{
	public static void main(String[]tejas){
		int rows=4;
		int num=1;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				System.out.print(num*num*num +" ");
				num++;
			}
			System.out.println();
		}
	}
}
