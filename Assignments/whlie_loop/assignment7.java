/* Write a program to print the countdown of days to submit the
assignment */

class Countdown{
	public static void main(String[]tejas){
		int i=7;
		while(i>=0){
			if(i>=1){
				System.out.println(i+" Days remaining");
			}else{
				System.out.println("0 days, Assignment is overdue");
			}
			i--;
		}
	}
}

