/* Write a program to print the sum of all even numbers and
multiplication of odd numbers between 1 to 10. */

class SumMult{
	public static void main(String[]tejas){
		long N=942111423;
		long sum=0;
		long mult=1;
		while(N!=0){
			long rem=N%10;
			if(rem%2!=0){
				sum=sum+rem;
			}
			else{
				mult=mult*rem;
			}
			N=N/10;
		}
		System.out.println("sum of odd number = "+sum);
		System.out.println("mult of even number = "+mult);
	}
}
