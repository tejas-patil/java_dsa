 /* Write a program to count the Odd digits of the given number. */

class OddDigit{
	public static void main(String[]tejas){
		long N=942111423;
		int count=0;
		while(N!=0){
			long rem=N%10;
			if(rem%2!=0){
				count++;
			}
			N=N/10;
		}
		System.out.println("Count of Odd Digits ="+count);
	}
}
