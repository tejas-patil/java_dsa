/* Write a program to print the square of even digits of the given number. */

class Square{
	public static void main(String[]tejas){
		long N=942111423;
		long square=1;
		while(N!=0){
			long rem=N%10;
			square=rem*rem;
			System.out.println(square);
			N=N/10;
		}
	}
}
